import React from 'react';
import {NavLink} from 'react-router-dom';
import  axios  from 'axios';
import { FormText } from 'reactstrap';

import './../../../assets/scss/style.scss';
import Aux from "../../../hoc/_Aux";
import Breadcrumb from "../../../App/layout/AdminLayout/Breadcrumb";

class SignUp1 extends React.Component {

    constructor(){
        super();
        this.state={
          email:"",
          emailErr:"",
          password:"",
          passwordErr:"",
          erreur:false
        }
    }
    
    //////fonction validate
      validate = () => {
    
        let isError = false;
    
        const errors = {
          emailErr: "",
          passwordErr: "",
        }
    
        const regex1=/^[a-zA-Z0-9._-]+$/;
    
        if ((this.state.email==="")||(this.state.email.length < 15)) {
          isError = true;
          errors.emailErr = "Veuillez verifier votre email";
        }
    
        if ((this.state.password==="")||(this.state.password.length > 20)||!regex1.test(this.state.password)) {
          isError = true;
          errors.passwordErr = "veuillez verifier votre mot de passe";
        }
    
        if (isError) {
          this.setState({
            ...this.state,
            ...errors
          })
        }
    
        this.setState({
          erreur:isError
        })
    
        return isError;
      }
    

    login(){
        let err=this.validate();
        if(!err) {;
          console.log("state", this.state);
          axios.post("http://localhost:5010/api/auth/login", {
            email: this.state.email,
            password: this.state.password
          }).then(res => {
            console.log("data ", res.data);
            if (res.data['data'] === null) {
              alert("votre E-mail ou mot de passe est invalide")
            }
           /* else if (res.data['data']['role'] === "Admin") {
              localStorage.setItem("idPer", res.data['data']['id'])
              
              console.log("c bon");
              console.log("data ", res.data);
             window.location.href = "/dashboard/default"
            }*/
            else {
                localStorage.setItem("user", JSON.stringify(res.data))
              
              console.log("c bon");
              console.log("data ", res.data);
              this.props.history.push("");
            // window.location.href = "/home"
             // alert("votre login ou mot de passe est invalide")
            }
          })
        }
    }

    render () {
        return(
            <Aux>
                <Breadcrumb/>
                <div className="auth-wrapper">
                    <div className="auth-content">
                        <div className="auth-bg">
                            <span className="r"/>
                            <span className="r s"/>
                            <span className="r s"/>
                            <span className="r"/>
                        </div>
                        <div className="card">
                            <div className="card-body text-center">
                                <div className="mb-4">
                                    <i className="feather icon-unlock auth-icon"/>
                                </div>
                                <h3 className="mb-4">Login</h3>
                                <div className="input-group mb-3">
                                    <input type="email" className="form-control" placeholder="Email" value={this.state.email}
                                        onChange={evt=>this.setState({email:evt.target.value})}/>
                                        {
                                            this.state.erreur===false ?
                                            <FormText>{this.state.emailErr}</FormText>:null
                                        }
                                        {
                                            this.state.erreur === true ?
                                            <FormText style={{color:"red"}}>{this.state.emailErr}</FormText> : null
                                        }
                                </div>
                                <div className="input-group mb-4">
                                    <input type="password" className="form-control" placeholder="password" value={this.state.password}
                                        onChange={evt=>this.setState({password:evt.target.value})}/>
                                        {
                                            this.state.erreur===false ?
                                            <FormText >{this.state.passwordErr}</FormText>:null
                                        }
                                        {
                                            this.state.erreur === true ?
                                            <FormText style={{color:"red"}}>{this.state.passwordErr}</FormText> : null
                                        }
                                </div>
                                <div className="form-group text-left">
                                    <div className="checkbox checkbox-fill d-inline">
                                        <input type="checkbox" name="checkbox-fill-1" id="checkbox-fill-a1" />
                                            <label htmlFor="checkbox-fill-a1" className="cr"> Save credentials</label>
                                    </div>
                                </div>
                                <button className="btn btn-primary shadow-2 mb-4" onClick={this.login.bind(this)}>Login</button>
                                <p className="mb-2 text-muted">Forgot password? <NavLink to="/auth/reset-password-1">Reset</NavLink></p>
                                <p className="mb-0 text-muted">Don’t have an account? <NavLink to="/auth/signup-1">Signup</NavLink></p>
                            </div>
                        </div>
                    </div>
                </div>
            </Aux>
        );
    }
}

export default SignUp1;